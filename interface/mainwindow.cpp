#include "mainwindow.h"

MainWindow::MainWindow(std::vector<int> _entrada, std::vector<std::vector<std::vector<int>>> _matriz3D, int _frames)
    : frames(_frames), vetorEntradas(_entrada), mapaFrames(_matriz3D)
{
    indexEntrada = 0;

    QWidget *window = new QWidget(this);
    this->setWindowTitle("Simulacao de Algoritmos de Substituicao de Paginas");
    this->setCentralWidget(window);
    QVBoxLayout *layoutJanela = new QVBoxLayout(window);

    //////// TOPO

    QLabel *quantidadeFrames = new QLabel("Frames: " + QString::number(frames), this);
    quantidadeFrames->setAlignment(Qt::AlignHCenter);
    QLabel *atualEntrada = new QLabel(QString::number(vetorEntradas[indexEntrada]), this);
    atualEntrada->setStyleSheet("QLabel { color : blue; }");
    atualEntrada->setAlignment(Qt::AlignHCenter);

    layoutJanela->addWidget(quantidadeFrames);
    layoutJanela->addWidget(atualEntrada);

    //////// ALGORITMOS

    layoutJanela->addLayout(LayoutAlgoritmos());

    //////// BOTOES

    labelsV.push_back(atualEntrada);
    layoutJanela->addLayout(LayoutBotoes());

    ////////

    layoutJanela->setAlignment(Qt::AlignHCenter);
    updateValores();
}

MainWindow::~MainWindow()
{
}

void MainWindow::updateValores()
{
    labelsV[3]->setText(QString::number(vetorEntradas[indexEntrada]));
    labelsV[4]->setText("Reference " + QString::number(indexEntrada + 1));
    std::vector<const char*> nomesV = {"FIFO", "LRU", "OPT"};
    for(int i = 0; i < (int)mapaFrames.size(); i++)
    {
        tablesV[i]->clear();
        labelsV[i]->setText(QString::fromStdString(nomesV[i]) + "\nPFs: " + QString::number(mapaFrames[i][indexEntrada][0]));
        for(int j = 1; j < (int)mapaFrames[i][indexEntrada].size(); j++)
        {
            if(mapaFrames[i][indexEntrada][j] == -1) break; 
            tablesV[i]->setItem(j-1,0, new QTableWidgetItem(QString::number(mapaFrames[i][indexEntrada][j])));
            if(indexEntrada < frames && (j-1) == indexEntrada) tablesV[i]->item(j-1,0)->setBackground(Qt::red);
            else if(mapaFrames[i][indexEntrada][j] != mapaFrames[i][indexEntrada-1][j]) tablesV[i]->item(j-1,0)->setBackground(Qt::red);
            else tablesV[i]->item(j-1,0)->setBackground(Qt::green);
        }
    }
}

void MainWindow::botaoClicado(int valor)
{
    bool houveMudanca = true;
    switch(valor)
    {
        case 0:
            if(indexEntrada == 0) houveMudanca = false;
            indexEntrada = 0;
            break;
        case 1:
            if(indexEntrada-1 >= 0) indexEntrada--;
            else houveMudanca = false;
            break;
        case 2:
            if(indexEntrada+1 < (int)vetorEntradas.size()) indexEntrada++;
            else houveMudanca = false;
            break;
        case 3:
            if(indexEntrada == (int)vetorEntradas.size() - 1) houveMudanca = false;
            indexEntrada = (int)vetorEntradas.size() - 1;
            break;
    }
    if(houveMudanca) updateValores();
}

QHBoxLayout* MainWindow::LayoutBotoes()
{
    QSignalMapper *signalMapper = new QSignalMapper(this);
    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(botaoClicado(int)));

    QHBoxLayout *layoutButtons = new QHBoxLayout();

    QPushButton *navegacaoPrimeiro = new QPushButton("First", this);
    buttonsV.push_back(navegacaoPrimeiro);
    signalMapper->setMapping(navegacaoPrimeiro, 0);
    connect(navegacaoPrimeiro, SIGNAL(clicked()), signalMapper, SLOT(map()));
    layoutButtons->addWidget(navegacaoPrimeiro);

    QPushButton *navegacaoAnterior = new QPushButton("Back", this);
    buttonsV.push_back(navegacaoAnterior);
    signalMapper->setMapping(navegacaoAnterior, 1);
    connect(navegacaoAnterior, SIGNAL(clicked()), signalMapper, SLOT(map()));
    layoutButtons->addWidget(navegacaoAnterior);

    QLabel *atualReferencia = new QLabel("Reference " + QString::number(indexEntrada + 1), this);
    labelsV.push_back(atualReferencia);
    layoutButtons->addWidget(atualReferencia);

    QPushButton *navegacaoProximo = new QPushButton("Next", this);
    buttonsV.push_back(navegacaoProximo);
    signalMapper->setMapping(navegacaoProximo, 2);
    connect(navegacaoProximo, SIGNAL(clicked()), signalMapper, SLOT(map()));
    layoutButtons->addWidget(navegacaoProximo);

    QPushButton *navegacaoUltimo = new QPushButton("Last", this);
    buttonsV.push_back(navegacaoUltimo);
    signalMapper->setMapping(navegacaoUltimo, 3);
    connect(navegacaoUltimo, SIGNAL(clicked()), signalMapper, SLOT(map()));
    layoutButtons->addWidget(navegacaoUltimo);

    return layoutButtons;
}

QHBoxLayout* MainWindow::LayoutAlgoritmos()
{
    QLabel *labelFIFO = new QLabel("FIFO\nPFs: ", this);
    labelsV.push_back(labelFIFO);
    labelFIFO->setAlignment(Qt::AlignHCenter);
    QTableWidget *tabelaFIFO = new QTableWidget(frames, 1, this);
    tablesV.push_back(tabelaFIFO);
    tabelaFIFO->setMaximumHeight(tabelaFIFO->rowHeight(0) * frames + 10);
    tabelaFIFO->setMaximumWidth(tabelaFIFO->columnWidth(0));
    tabelaFIFO->verticalHeader()->setVisible(false);
    tabelaFIFO->horizontalHeader()->setVisible(false);
    QVBoxLayout *layoutFIFO = new QVBoxLayout();
    layoutFIFO->addWidget(labelFIFO);
    layoutFIFO->addWidget(tabelaFIFO);

    QLabel *labelLRU = new QLabel("LRU\nPFs: ", this);
    labelsV.push_back(labelLRU);
    labelLRU->setAlignment(Qt::AlignHCenter);
    QTableWidget *tabelaLRU = new QTableWidget(frames, 1, this);
    tablesV.push_back(tabelaLRU);
    tabelaLRU->setMaximumHeight(tabelaLRU->rowHeight(0) * frames + 10 );
    tabelaLRU->setMaximumWidth(tabelaLRU->columnWidth(0));
    tabelaLRU->verticalHeader()->setVisible(false);
    tabelaLRU->horizontalHeader()->setVisible(false);
    QVBoxLayout *layoutLRU = new QVBoxLayout();
    layoutLRU->addWidget(labelLRU);
    layoutLRU->addWidget(tabelaLRU);

    QLabel *labelOPT = new QLabel("OPT\nPFs: ", this);
    labelsV.push_back(labelOPT);
    labelOPT->setAlignment(Qt::AlignHCenter);
    QTableWidget *tabelaOPT = new QTableWidget(frames, 1, this);
    tablesV.push_back(tabelaOPT);
    tabelaOPT->setMaximumHeight((tabelaOPT->rowHeight(0) * frames) + 10);
    tabelaOPT->setMaximumWidth(tabelaOPT->columnWidth(0));
    tabelaOPT->verticalHeader()->setVisible(false);
    tabelaOPT->horizontalHeader()->setVisible(false);
    QVBoxLayout *layoutOPT = new QVBoxLayout();
    layoutOPT->addWidget(labelOPT);
    layoutOPT->addWidget(tabelaOPT);

    QHBoxLayout *layoutAlgoritmos = new QHBoxLayout();

    layoutAlgoritmos->addLayout(layoutFIFO);
    layoutAlgoritmos->addLayout(layoutLRU);
    layoutAlgoritmos->addLayout(layoutOPT);

    return layoutAlgoritmos;
}
