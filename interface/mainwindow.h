#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QLayout>
#include <QLabel>
#include <QTableWidget>
#include <QHeaderView>
#include <QSignalMapper>

    //--------------------------------------------
    //                  CLASSE MAINWINDOW
    //
    //      Possui metodos utilizados pelo Qt que
    //  geram as labels, tabelas e push buttons
    //  para navegacao e demonstracao dos valores
    //  de execucao resultantes dos algoritmos.
    //--------------------------------------------

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    //--------------------------------------------
    //              CONSTRUTOR
    MainWindow(std::vector<int>, std::vector<std::vector<std::vector<int>>>, int);
    //      Construtor que recebe o vetor de entrada,
    //  matriz contendo os resultados de cada execucao
    //  e a quantidade de frames.
    //      Define o widget principal do programa,
    //  que contera todos os layouts definidos pelas
    //  funcoes abaixo.
    //--------------------------------------------
    //              DESTRUTOR
    ~MainWindow();
    //      Destrutor padrao.
    //--------------------------------------------
    //              UPDATEVALORES
    void updateValores();
    //      Metodo que realiza a blindagem,
    //  organizacao, definicao de valores e formatacao
    //  das tabelas de algoritmos e labels que mudam
    //  de valor durante a execucao.
    //--------------------------------------------
    //              LAYOUTBOTOES
    QHBoxLayout* LayoutBotoes();
    //      Metodo que gera, conecta e organiza
    //  os botoes utilizados para navegar pelas
    //  referencias. Conecta os botoes ao slot
    //  botaoClicado com o sinal do mapper.
    //--------------------------------------------
    //              LAYOUTALGORITMOS
    QHBoxLayout* LayoutAlgoritmos();
    //      Metodo que gera e organiza a parte
    //  que demonstra os resultados de cada
    //  algoritmo, como tabelas e labels.
    //--------------------------------------------

private:
    int indexEntrada;

    enum Algoritmos {FIFO, LRU, OPT};

    const int frames;
    const std::vector<int> vetorEntradas;
    const std::vector<std::vector<std::vector<int>>> mapaFrames;

    std::vector<QLabel*> labelsV;
    std::vector<QPushButton*> buttonsV;
    std::vector<QTableWidget*> tablesV;

private slots:
    //--------------------------------------------
    //              BOTAOCLICADO
    void botaoClicado(int);
    //      Slot usado pelos push buttons, quando
    //  um botao eh clicado esse metodo eh chamado.
    //      Recebe um inteiro com a identificacao
    //  do botao que foi apertado, entao aumenta,
    //  diminui, zera, ou coloca indexEntrada no
    //  valor maximo.
    //      Apos definir o novo valor de indexEntrada
    //  chama o metodo updateValores.
    //--------------------------------------------
};
#endif // MAINWINDOW_H
