QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

DEFINES += COMPILAR_QT=1

SOURCES += \
    ../src/main.cpp \
    ../src/simulador.cpp \
    ../src/toolbox.cpp \
    mainwindow.cpp \

HEADERS += \
    ../include/simulador.hpp \
    ../include/toolbox.hpp \
    mainwindow.h \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
