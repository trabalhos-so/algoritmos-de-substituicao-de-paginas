#include "../include/simulador.hpp"
#include "../include/toolbox.hpp"

#if COMPILAR_QT == 1
    #include "../interface/mainwindow.h"
    #include <QApplication>
#endif

int main(int argc, char* argv[])
{
    if(argc != 2 || std::stoi(argv[1],nullptr) <= 0)
    {
        std::cerr << "\033[0;31mExecute o programa conforme as instrucoes do arquivo README!\033[0m\n";
        return 1;
    }
    int frames = std::stoi(argv[1],nullptr);
    Toolbox objToolbox;
    std::vector<int> vetorEntrada = objToolbox.leituraReferencia();
    if(vetorEntrada.empty() == true) return 1;
    Simulador objSimulador(frames, vetorEntrada);
    objToolbox.printOutput(frames, vetorEntrada.size(), objSimulador.getResultados());
    #if COMPILAR_QT == 1
        QApplication a(argc, argv);
        MainWindow w(vetorEntrada, objSimulador.getMatrizExecucao(), frames);
        w.show();
        return a.exec();
    #endif
    return 0;
}