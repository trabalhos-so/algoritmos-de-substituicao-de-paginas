#include "../include/toolbox.hpp"

Toolbox::Toolbox()
{}

Toolbox::~Toolbox()
{}

std::vector<int> Toolbox::leituraReferencia()
{
    std::string linhaEntrada;
    std::vector<int> vetorEntrada;
    while(std::getline(std::cin, linhaEntrada))
    {
        try
        {
            if(linhaEntrada == "\0") break;
            int referencia = std::stoi(linhaEntrada, nullptr);
            if(referencia >= 0) vetorEntrada.push_back(referencia);
            else {
                std::cerr << "Valor invalido na entrada!\nLinha: " << vetorEntrada.size()+1 << std::endl;
                vetorEntrada.clear();
                break;
            }
        }
        catch(std::invalid_argument& e)
        {
            std::cerr << "Valor invalido na entrada!\nLinha: " << vetorEntrada.size()+1 << std::endl;
            vetorEntrada.clear();
            break;
        }
    }
    return vetorEntrada;
}

void Toolbox::printVetor(std::vector<int> vetor)
{
    for(unsigned int i = 0; i < vetor.size(); i++) std::cout << vetor[i] << " ";
    std::cout << "\n";
}

void Toolbox::printMatriz(std::vector<std::vector<int>> matriz)
{
    for(unsigned int i = 0; i < matriz.size(); i++)
    {
        for(unsigned int j = 0; j < matriz[i].size(); j++)
        {
            std::cout << matriz[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

void Toolbox::printOutput(int quadros, int qtdReferencias, std::vector<int> vetor)
{
    std::cout << quadros << " quadros\n" << qtdReferencias << " refs\n";
    std::vector<std::string> algoritmos = {"FIFO", "LRU", "OPT"};
    for(unsigned int i = 0; i < vetor.size(); i++) std::cout << algoritmos[i] << ": " << vetor[i] << " PFs\n";
}