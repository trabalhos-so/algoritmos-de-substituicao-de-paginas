#include "../include/simulador.hpp"
#include "../include/toolbox.hpp"

Simulador::Simulador(int quadros, std::vector<int> vetorArg) : frames(quadros), vetorEntrada(vetorArg)
{
    FirstInFirstOut();
    LeastRecentlyUsed();
    AlgoritmoOtimo();
}

Simulador::~Simulador()
{
}

std::vector<int> Simulador::getResultados()
{
    return vetorResultado;
}

#if COMPILAR_QT == 1
    std::vector<std::vector<std::vector<int>>> Simulador::getMatrizExecucao()
    {
        return matrizExecucao;
    }
#endif

void Simulador::FirstInFirstOut()
{
    #if COMPILAR_QT == 1
        std::vector<std::vector<int>> matrizExecucaoAtual;
    #endif
    std::vector<int> vetorFrames(frames, -1);
    int cont = 0, pfs = 0, contador = 0;
    bool processoAberto = false;
    while(contador < (int)vetorEntrada.size())
    {
        for(int i = 0; i < frames; i++)
        {
            if(vetorFrames[i] == vetorEntrada[contador])
            {
                processoAberto = true;
                break;
            }
            else
            {
                processoAberto = false;
            }
                
        }
            if(processoAberto != true)
            {
                vetorFrames[cont] = vetorEntrada[contador];
                cont++;
                pfs++;
                processoAberto = false;
                if(cont == frames) cont = 0;
            }
        #if COMPILAR_QT == 1
            matrizExecucaoAtual.push_back(vetorFrames);
            matrizExecucaoAtual[matrizExecucaoAtual.size() - 1].insert(matrizExecucaoAtual[matrizExecucaoAtual.size() - 1].begin(), pfs);
        #endif
        contador++;
    }
    #if COMPILAR_QT == 1
        matrizExecucao.push_back(matrizExecucaoAtual);
    #endif
    vetorResultado.push_back(pfs); 
}



void Simulador::LeastRecentlyUsed()
{
    #if COMPILAR_QT == 1
        std::vector<std::vector<int>> matrizExecucaoAtual;
    #endif
    std::vector<int> vetorFrames(frames, -1);
    std::vector<int> vetorPosicao;
    int cont = 0, pfs = 0, contador = 0, frame;
    bool processoAberto = false, frameCheio = false;
    while(contador < (int)vetorEntrada.size())
    {
        for(unsigned int i = 0; i < vetorPosicao.size(); i++)
        {
            if(vetorPosicao[i] == vetorEntrada[contador])
            {
                vetorPosicao.erase(vetorPosicao.begin()+i);
                processoAberto = true;
                for(int j = 0; j < frames; j++)
                {
                    if(frameCheio == false)break;
                    if(vetorFrames[j] == vetorEntrada[contador])frame = j;
                }
                break;
            }
            else
            {
                processoAberto = false;
            }
        }
        if((processoAberto == true && frameCheio == false) || (processoAberto == true && frameCheio == true))
        {
            vetorPosicao.push_back(vetorEntrada[contador]);
        }

        if(processoAberto == false && frameCheio == true){
            for(int j = 0; j < frames; j++)
            {
                if(vetorPosicao[0] == vetorFrames[j])frame = j;
            }
            vetorPosicao.erase(vetorPosicao.begin());
            vetorFrames[frame] = vetorEntrada[contador];
            pfs++;
            vetorPosicao.push_back(vetorEntrada[contador]);
        }
            
        if(processoAberto == false && frameCheio == false)
        {
            vetorFrames[cont] = vetorEntrada[contador];
            vetorPosicao.push_back(vetorEntrada[contador]);
            pfs++;
            cont++;
            if(cont == frames)
            {
                frameCheio = true;
                cont = 0;
            }
        }
        #if COMPILAR_QT == 1
            matrizExecucaoAtual.push_back(vetorFrames);
            matrizExecucaoAtual[matrizExecucaoAtual.size() - 1].insert(matrizExecucaoAtual[matrizExecucaoAtual.size() - 1].begin(), pfs);
        #endif
        contador++;
    }
    #if COMPILAR_QT == 1
        matrizExecucao.push_back(matrizExecucaoAtual);
    #endif
    vetorResultado.push_back(pfs);

}

void Simulador::AlgoritmoOtimo()
{
    #if COMPILAR_QT == 1
        std::vector<std::vector<int>> matrizExecucaoAtual;
    #endif
    std::unordered_map<int, std::vector<int>> mapa;
    for(int i = 0; i < (int)vetorEntrada.size(); i++)
    {
        auto search = mapa.find(vetorEntrada[i]);
        if(search != mapa.end())
        {
            mapa[vetorEntrada[i]].push_back(i);
        } else {
            mapa.insert(std::pair<int, std::vector<int>>(vetorEntrada[i], std::vector<int>()));
            mapa[vetorEntrada[i]].push_back(i);
        }
    }

    int contadorEntrada = 0, pfs = 0;
    std::vector<int> vetorFrames;
    while(contadorEntrada < (int)vetorEntrada.size())
    {
        bool processoAberto = false;
        int maisLonge = 0, maisLongeFrame = -1, naoApareceMais = -1;
        for(int i = 0; i < (int)vetorFrames.size(); i++)
        {
            if(vetorFrames[i] == vetorEntrada[contadorEntrada])
            {
                processoAberto = true;
                break;
            } else {
                processoAberto = false;
            }
        }
        if(processoAberto == false)
        {
            if((int)vetorFrames.size() < frames) vetorFrames.push_back(vetorEntrada[contadorEntrada]);
            else {
                for(int i = 0; i < (int)vetorFrames.size(); i++)
                {
                    if((int)vetorFrames.size() == frames)
                    {
                        std::vector<int> vectorFrameElemento = mapa[vetorFrames[i]];
                        while(vectorFrameElemento.empty() != true && vectorFrameElemento[0] < contadorEntrada) vectorFrameElemento.erase(vectorFrameElemento.begin());
                        if(vectorFrameElemento.empty())
                        {
                            if(naoApareceMais == -1)
                            {
                                naoApareceMais = vetorFrames[i];
                                maisLongeFrame = i;
                            }
                        }
                        else if(naoApareceMais == -1 && vectorFrameElemento[0] > maisLonge)
                        {
                            maisLonge = vectorFrameElemento[0];
                            maisLongeFrame = i;
                        }
                        mapa[vetorFrames[i]] = vectorFrameElemento;
                    }
                }
                vetorFrames[maisLongeFrame] = vetorEntrada[contadorEntrada];
            }
            pfs++;
        }
        #if COMPILAR_QT == 1
            matrizExecucaoAtual.push_back(vetorFrames);
            matrizExecucaoAtual[matrizExecucaoAtual.size() - 1].insert(matrizExecucaoAtual[matrizExecucaoAtual.size() - 1].begin(), pfs);
        #endif
        contadorEntrada++;
    }
    #if COMPILAR_QT == 1
        matrizExecucao.push_back(matrizExecucaoAtual);
    #endif
    vetorResultado.push_back(pfs);
}
