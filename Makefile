PROJ=simulador
SRC=$(shell find ./src/ -name "*.cpp" -type f)
$(shell mkdir -p objects)
OBJ=$(patsubst ./src%, ./objects%, $(patsubst %.cpp, %.o , $(SRC)))
FLAGS=-Wall -Wextra -Wshadow -Werror -I include -DCOMPILAR_QT=0

$(PROJ): $(OBJ)
	g++ -o $@ $^ $(FLAGS)

objects/%.o: src/%.cpp
	g++ -c -o $@ $^ $(FLAGS)

exemplos:
	./simulador $(FRAMES) < vsim-belady.txt
	./simulador $(FRAMES) < vsim-exemplo.txt
	./simulador $(FRAMES) < vsim-gcc.txt

clean:
	@find . -type f -name "*.o" -exec rm '{}' \;
	@find . -type f -name "$(PROJ)" -exec rm '{}' \;
	@rmdir objects;

valgrind:
	valgrind --leak-check=full --show-leak-kinds=all ./simulador $(FRAMES) < vsim-belady.txt
	valgrind --leak-check=full --show-leak-kinds=all ./simulador $(FRAMES) < vsim-exemplo.txt
	valgrind --leak-check=full --show-leak-kinds=all ./simulador $(FRAMES) < vsim-gcc.txt

gui:
	cd interface && qmake && make
	./interface/simulador $(FRAMES) < $(FILE)