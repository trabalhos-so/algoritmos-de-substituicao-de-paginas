# Simulação de Algoritmos de Escalonamento

Programa que tem por objetivo simular o comportamento de algoritmos de substituição de páginas.

# Alunos

* Bernardo Dalfovo de Souza - 18204849
* Vinicius Slovinski        - 18201356

# Algoritmos suportados

* FIFO (First In, First Out);
* LRU (Least Recently Used);
* OPT (Algoritmo ótimo).

# Formato dos arquivos de referência

* Uma referência por linha;
* Números inteiros positivos;
* Arquivo ".txt" localizado na pasta principal do projeto.

# Output

* Número de quadros (frames) disponíveis na memória RAM;
* Quantidade de referências;
* Resultado para FIFO;
* Resultado para LRU;
* Resultado para OPT.

# Como compilar:

* Instale as dependências pelo terminal com o comando:

```bash
sudo apt install make g++ build-essential
```

* Digite ```make``` no terminal quando estiver na pasta do projeto.

# Como executar:

```./simulador nFrames < arquivo.txt```

* nFrames: número de quadros (frames) disponíveis na memória RAM;
* arquivo.txt: arquivo de referência às páginas.

# Outros comandos "make":

* ```make exemplos FRAMES=qtdFrames``` : Executa os três algoritmos em sequência com as referências de exemplo, substitua "qtdFrames" por um número maior que zero que representará a quantidade de quadros disponíveis na memória RAM.

* ```make clean``` : Limpa os arquivos executaveis e objetos;

* ```make valgrind FRAMES=qtdFrames``` : Executa o programa valgrind para a verificação de vazamento de memória nos três algoritmos com as referências de exemplo, substitua "qtdFrames" por um número maior que zero que representará a quantidade de quadros disponíveis na memória RAM.

* ```make gui FRAMES=qtdFrames FILE=arquivo.txt``` : Executa o programa com a interface gráfica do Qt, necessário instalar as dependências listadas abaixo para a execução apropriada. Substitua "qtdFrames" por um número maior que zero que representará a quantidade de quadros disponíveis na memória RAM e "arquivo.txt" pelo nome do arquivo de referências localizado na pasta principal do projeto.

# Como executar com interface gráfica (Linux):

* Instale as dependências pelo terminal com o comando:

```bash
sudo apt install qt5-default
```

* Compile o projeto do Qt utilizando QMake e Makefile:

```bash
cd interface && qmake && make && cd ..
```

* Execute o programa:
    * nFrames: número de quadros (frames) disponíveis na memória RAM;
    * arquivo.txt: arquivo de referência às páginas.

```./interface/simulador nFrames < arquivo.txt```
