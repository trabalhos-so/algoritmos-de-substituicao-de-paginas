#ifndef SIMULADOR_HPP
#define SIMULADOR_HPP

#include <string>
#include <vector>
#include <unordered_map>

        //--------------------------------------------
        //              CLASSE SIMULADOR
        //      Classe que possui a implementacao dos
        //  algoritmos de substituicao de paginas
        //  para a realizacao da simulacao.
        //--------------------------------------------

class Simulador
{
    public:
        //--------------------------------------------
        //                  CONSTRUTOR
        Simulador(int, std::vector<int>);
        //      Construtor executa as funcoes de 
        //  algoritmos em ordem.
        //--------------------------------------------
        //                  DESTRUTOR
        ~Simulador();
        //      Destrutor padrao.
        //--------------------------------------------
        //          METODO GETRESULTADOS
        std::vector<int> getResultados();
        //      Retorna os valores de interesse que 
        //  foram obtidos durante a execucao de cada
        //  algoritmo e passados no final deles.
    private:
        //--------------------------------------------
        //          METODO FIRSTINFIRSTOUT
        void FirstInFirstOut();
        //      Como a logica do FIFO e mais simples,
        //  sua implementacao nao e complexa. A ideia 
        //  sera que a posicao da vez pra entrar no 
        //  vetor frames, sempre apontara para a 
        //  pagina que entrou primeiro. O vetor     
        //  de entrada sera lido completamente, enquanto
        //  isso, os frames serao preenchidos por paginas
        //  que nao estao abertos. O local de preenchimento
        //  no vetor vai de 0 ate o numero maximo de frames
        //  e repete esse processo. Dessa maneira garante-se
        //  a rotatitividade dos frames. Quando uma pagina 
        //  ja estiver aberto ele nao vai entrar de novo
        //  nos frames, entao a posicao de entrada nos 
        //  frames nao deve ser alterada, e continura 
        //  apontando para a pagina que esta ali a 
        //  mais tempo.
        //--------------------------------------------
        //          METODO LEASTRECENTLYUSED
        void LeastRecentlyUsed();
        //      Para implementar o LRU, utiliza-se um
        //  vetor secundario para ordenar as paginas
        //  e definir qual sera substituida. Nesse vetor
        //  sempre que uma pagina nao aberto for chamado,
        //  ele ira para o final do vetor, o primeiro 
        //  elemento desse vetor sera retirado do vetor
        //  frames e a nova pagina colocada nessa 
        //  posicao que era ocupada.
        //      Porem, quando uma pagina aberta for
        //  chamada, sua posicao no vetor secundario e 
        //  encontrada e excluida, para entao ser colocada
        //  novamente no final desse vetor. Nessa caso 
        //  nao havera substituicao no vetor frames, 
        //  apenas uma renovacao da pagina chamada.
        //      Enquanto o vetor frames nao estiver
        //  completo, a posicao de escolha para novas
        //  paginas, mantem-se na proxima posicao
        //  vazia, a renovacao das paginas funciona
        //  da mesma maneira, mas nenhuma pagina sera 
        //  substituida antes que o todos os frames
        //  estejam completos. 
        //--------------------------------------------
        //          METODO ALGORTIMOOTIMO
        void AlgoritmoOtimo();
        //      O algoritmo otimo so pode ser implentado
        //  pois e passado o vetor de entradas. Esse vetor
        //  e lido completamente uma unica vez, e as 
        //  posicoes de entrada de cada pagina sao
        //  salvas num unordered_map. Dessa maneira 
        //  obtem-se um acesso muito mais agil a esses
        //  elementos, o que impacta consideravelmente
        //  quando o vetor de entradas e muito grande.
        //      Entao quando uma pagina nao aberta e
        //  chamada pelo vetor entrada, e feita a 
        //  conferencia das paginas que estao no 
        //  frame sobre qual e a pagina que tem
        //  o maior valor de posicao a ser chamado.
        //      Essa pagina sera substituida no vetor
        //  frames pela nova pagina.
        //--------------------------------------------

        const int frames;
        const std::vector<int> vetorEntrada;
        std::vector<int> vetorResultado;
        #if COMPILAR_QT == 1
            //--------------------------------------------
            //          METODO GETMATRIZEXECUCAO
            public: std::vector<std::vector<std::vector<int>>> getMatrizExecucao();
            //      Retorna os valores obtidos na execucao,
            //  uma vez que eles sao colocados na matriz
            //  no decorrer da execucao de cada algoritmo.
            private: std::vector<std::vector<std::vector<int>>> matrizExecucao;
        #endif
};

#endif
