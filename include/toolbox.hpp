#ifndef TOOLBOX_HPP
#define TOOLBOX_HPP

#include <vector>
#include <queue>
#include <iostream>
#include <string>
#include <istream>
#include <stdexcept>

        //--------------------------------------------
        //                  CLASSE TOOLBOX
        //
        //      Possui metodos utilizados pela main que
        //  nao envolvem os algoritmos diretamente.
        //--------------------------------------------

class Toolbox
{
    public:
        //--------------------------------------------
        //                  CONSTRUTOR
        Toolbox();
        //      Construtor padrao.
        //--------------------------------------------
        //                  DESTRUTOR
        ~Toolbox();
        //      Destrutor padrao.
        //--------------------------------------------
        //              METODO LEITURAREFERENCIA
        std::vector<int> leituraReferencia();
        //      Realiza a leitura arquivo txt.
        //      Retorna um vetor de inteiros no qual cada
        //  linha representa uma referencia.
        //--------------------------------------------
        //              METODO PRINTVETOR
        void printVetor(std::vector<int>);
        //      Imprime o vetor informado pelo parametro.
        //--------------------------------------------
        //              METODO PRINTMATRIZ
        void printMatriz(std::vector<std::vector<int>>);
        //      Imprime a matriz informada pelo parametro.
        //--------------------------------------------
        //              METODO PRINTOUTPUT
        void printOutput(int, int, std::vector<int>);
        //      Imprime o resultado da execucao
        //  com a formatacao correta.
        //--------------------------------------------
};

#endif